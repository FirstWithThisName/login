/*
 * project: apache
 * created: Thu Jan 11 23:35:26 2018
 * creator: christian
*/

#include "apache_requestHeader.h"

#include <stdlib.h>
#include <string.h>

int apache_getContentLength() {
    char *env = getenv("CONTENT_LENGTH");

    if (env == NULL) {
        return 0;
    } else {
        return atoi(env);
    }
}
