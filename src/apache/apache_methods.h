#ifndef _APACHE_METHODS_H_
#define _APACHE_METHODS_H_

/*
 * project: apache
 * created: Fri Jan 12 14:07:41 2018
 * creator: christian
*/


/**************** START PROTOTYPING ****************/

#include <stdlib.h>
#include <stdio.h>
char *apache_getPost();

#include <stdlib.h>
#include <stdio.h>
char *apache_getGet();

/**************** END   PROTOTYPING ****************/

#endif /* apache_methods.h */
