#ifndef _APACHE_PARSE_H_
#define _APACHE_PARSE_H_

/*
 * project: apache
 * created: Fri Jan 12 11:04:16 2018
 * creator: christian
*/


typedef struct {
	char *key;
	char *value;
} cgiData_t;

/**************** START PROTOTYPING ****************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../llist/llist.h"

// TODO BUG BUGGY does not escape the & !!!
llist *apache_parse(char *buffer);

/**************** END   PROTOTYPING ****************/

#endif /* apache_parse.h */
