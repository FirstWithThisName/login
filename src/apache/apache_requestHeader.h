#ifndef _HTTP
#define _HTTP

/*
 * project: apache
 * created: Thu Jan 11 23:35:22 2018
 * creator: christian
*/


/**************** START PROTOTYPING ****************/

int apache_getContentLength();

/**************** END   PROTOTYPING ****************/

#endif /* http_header.h */
