/*
 * project: apache
 * created: Fri Jan 12 11:04:13 2018
 * creator: christian
*/

#include "apache_parse.h"

char convert(char *hex) {
   char ascii;

   /* erster Hexawert */
   ascii =
   (hex[0] >= 'A' ? ((hex[0] & 0xdf) - 'A')+10 : (hex[0] - '0'));

   ascii <<= 4; /* Bitverschiebung schneller als ascii*=16 */
   /* zweiter Hexawert */
   ascii +=
   (hex[1] >= 'A' ? ((hex[1] & 0xdf) - 'A')+10 : (hex[1] - '0'));
   return ascii;
}

void hex2ascii(char *str) {
   int x,y;

   for(x=0,y=0; str[y] != '\0'; ++x,++y) {
      str[x] = str[y];
      /* Ein hexadezimales Zeichen? */
      if(str[x] == '%')  {
         str[x] = convert(&str[y+1]);
         y += 2;
      }
      /* Ein Leerzeichen? */
      else if( str[x] == '+')
         str[x]=' ';
   }
   /* geparsten String sauber terminieren */
   str[x] = '\0';
}


llist *apache_parse(char *buffer) {
	llist *list =  llist_create(NULL, NULL, 0);
	if (list == NULL)
		return NULL;
	hex2ascii(buffer);
	char *pair = strtok(buffer, "&");
	while (pair != NULL) {
		cgiData_t *keyValue = malloc(sizeof(cgiData_t));

		char *at = strchr(pair, '=');
		if (at == NULL) {
			keyValue->value = NULL;
			keyValue->key = malloc(strlen(pair));
			strcpy(keyValue->key, pair);
			keyValue->key[strlen(pair)] = '\0';
		} else {
			keyValue->value = malloc(strlen(at));
			if (!keyValue) return NULL;
			strcpy(keyValue->value, at+1);
			keyValue->key = malloc(at - pair);
			strncpy(keyValue->key, pair, at - pair);
			keyValue->key[at - pair] = '\0';
		}


		if (llist_add_node(list, keyValue, ADD_NODE_FRONT) != LLIST_SUCCESS) return NULL;

		pair = strtok(NULL, "&");
	}

	return list;
}
