#ifndef _APACHE
#define _APACHE

/*
 * project: apache
 * created: Thu Jan 11 23:49:07 2018
 * creator: christian
*/

/*
200 alles okay

201 POST-Befehl erfolgreich

202 Anforderung akzeptiert

203 GET-Anforderung erfüllt

204 Anforderung erfüllt, aber Rücksendung nicht verstanden

300 Datenquelle an mehreren Stellen gefunden

301 Datenquelle war dauernd in Bewegung.

302 Datenquelle war zeitweise in Bewegung.

304 Datenquelle konnte nicht näher bestimmt werden.

400 unverständliche Anforderung vom Client

401 ein File wurde angefragt, für das sich der User ausweisen muss

403 Anfrage war verständlich, der Server weigert sich jedoch, das Dokument zu senden, da dieser oder der Client keine Berechtigung hat. Beispiel: keine Leserechte der Datei.

404 Datenquelle nicht gefunden

405 Verfahren an Datenquelle nicht erlaubt. Zum Beispiel ist die Abfrage-Methode POST explizit gesperrt.

406 Art der Datenquelle nicht erwünscht

408 Anfrage Timeout -> Server überlastet? Fehlkonfiguriert?

500 Fehler im CGI-Skript, falsche Zugriffsrechte, Server falsch konfiguriert

501 Anfrage wird vom Server nicht unterstützt, da notwendige Module nicht implementiert sind.

502 schlechte Netzverbindung oder Server überladen

503 Dienst steht nicht zur Verfügung – Timeout, wenn z. B. ein Datenbankserver nicht reagiert.
*/


#endif /* apache_httpStatus.h */
