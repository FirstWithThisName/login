#ifndef _CODE_GENERATOR_
#define _CODE_GENERATOR_

/*
 * project: auth
 * created: Thu Sep 21 10:54:33 2017
 * creator: christian
*/

#include <stdint.h>

#include "base32.h"
#include "hmac.h"
#include "sha1.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BITS_PER_BASE32_CHAR      5           // Base32 expands space by 8/5
#define VERIFICATION_CODE_MODULUS 1000000			// Six digits

/**************** START PROTOTYPING ****************/

extern int generateCode(const char *key, unsigned long tm);

/**************** END   PROTOTYPING ****************/

#endif /* code_generate.h */
