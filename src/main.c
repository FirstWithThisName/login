/*
 * project: src
 * created: Thu Jan 11 21:34:14 2018
 * creator: christian
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "llist/llist.h"

#include "chtml/html_basics.h"
#include "chtml/html_media.h"

#include "apache/apache_requestHeader.h"
#include "apache/apache_parse.h"
#include "apache/apache_methods.h"

bool paswd = false;
bool email = false;
bool otp = false;

void test(void *data) {
	cgiData_t key =  *((cgiData_t *)data);
	printf("<p>key: %s|value: %s</p>\n", key.key, key.value);
	if (strcmp("password", key.key) == 0) {
		if (strcmp(key.value, "test123") == 0) {
			paswd = true;
		} else {
			paswd = false;
			printf("pass\n");
		}
	} else if (strcmp("email", key.key) == 0) {
		if (strcmp(key.value, "test@test.de") == 0) {
			email = true;
		} else {
			email = false;
			printf("email\n");
		}
	} else if (strcmp("otp", key.key) == 0) {
		if (strcmp(key.value, "12345") == 0) {
			otp = true;
		} else {
			otp = false;
			printf("otp\n");
		}
	}

}

int main(int argc, char *argv[]) {
	html_print_header("login", "css/login.css", NULL);

	char *buffer = apache_getPost();

	if (buffer) {
		llist *data = apache_parse(buffer);
		llist_for_each(data, test);
		if (otp) {
			printf("<h1> Your are logged in</h1>\n");
			html_print_picture("this picture is your price", "html/cat.jpeg", NULL, "price");
		} else {
			printf("<h1>Login Failed</h1>\n");
			paswd = false;
			email = false;
			otp = false;
		}
	} else {
		printf("<h1>Login</h1>\n");
		html_startDiv(NULL, "login");
		html_loadElement("html/login.html");
		html_endDiv();

	}
	html_print_end();

	return 0;
}
