#ifndef _CHTML_MEDIA_H_
#define _CHTML_MEDIA_H_

/*
 * project: mediaserver
 * created: Mon Oct 30 02:24:30 2017
 * creator: christian
*/

#include <stdio.h>
#include <stdlib.h>
#include "html_basics.h"

/**************** START PROTOTYPING ****************/

void html_print_music(const char *title, const char *path, const char *desc, const char *mclass, const char *thumpnale);

void html_print_picture(const char *title, const char *path, const char *desc , const char *pclass);

void html_print_video(const char *title, const char *path, int width, int height, const char *desc, const char *vclass);

/**************** END   PROTOTYPING ****************/

#endif /* html_media.h */
