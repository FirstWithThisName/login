/*
 * project: mediaserver
 * created: Mon Oct 30 02:24:32 2017
 * creator: christian
*/

#include "html_media.h"

void html_print_picture(const char *title, const char *path, const char *desc, const char *pclass) {
	html_startDiv(pclass, NULL);
	printf("<p class=\"title\">%s</p>\n", title);
	printf("<img src=\"%s\"/>\n", path);
	printf("<p class=\"description\">%s</p>\n", desc);
	html_endDiv();
}

void html_print_music(const char *title, const char *path, const char *desc, const char *mclass, const char *thumpnale) {
	html_startDiv(mclass, NULL);
	printf("<p class=\"title\">%s</p>\n", title);
    if (thumpnale) {
        printf("<center>\n");
        printf("<img src=\"%s\">\n", thumpnale);
        printf("</center>\n");
    }
	printf("<audio controls>\n");
	printf("<source src=\"%s\" type=\"audio/mp3\">\n", path);
	printf("Your browser does not support the audio element.\n");
	printf("</audio>\n");
	printf("<p class=\"description\">%s</p>\n", desc);
	html_endDiv();
}

void html_print_video(const char *title, const char *path, int width, int height, const char *desc, const char *vclass) {
    html_startDiv(vclass, NULL);
	printf("<p class=\"title\">%s</p>\n", title);
	printf("<video width=\"%d\" height=\"%d\" controls>\n", width, height);
  	printf("<source src=\"%s\" type=\"video/mp4\">\n", path);
	printf("Your browser does not support the video tag.\n");
	printf("</video> \n");
	printf("<p class=\"description\">%s</p>\n", desc);
	html_endDiv();
}
