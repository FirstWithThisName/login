#ifndef _CHTML
#define _CHTML

/*
 * project: mediaserver
 * created: Tue Oct 17 21:25:55 2017
 * creator: christian
*/

#include <stdio.h>

/**************** START PROTOTYPING ****************/

void html_print_header(char *title, const char *css, const char *js);

void html_print_end(void);

int html_loadElement(const char *htmlFile);

void html_startDiv(const char *class, const char *id);

void html_endDiv();

void html_noResults();

void html_print_footer(unsigned int next, unsigned int back, const char *fdi, const char *content, const char *search);

/**************** END   PROTOTYPING ****************/

#endif /* chtml/html_basics.h */
