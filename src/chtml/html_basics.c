/*
 * project: mediaserver
 * created: Tue Oct 17 21:25:50 2017
 * creator: christian
*/

#include "html_basics.h"

void html_print_header(char *title, const char *css, const char *js) {
	printf("Content-Type: text/html\n\n");
	printf("<!DOCTYPE html>\n");

	printf("<html><head>\n");
	if (title)
		printf("<title>%s</title>\n",title);

	printf("<meta charset=\"UTF-8\">");
	if (css)
		printf("<link rel=\"stylesheet\" type=\"text/css\" href=\"%s\" />\n", css);

	if (js)
		printf("<script language=\"javascript\" type=\"text/javascript\" src=\"%s\"></script>\n", js);
	printf("</head>\n<body>\n");
}

void html_print_end(void) {
	printf("</body></html>\n");
}

void html_startDiv(const char *class, const char *id) {
    printf("<div");

    if (class)
        printf(" class=\"%s\"", class);
    if (id)
        printf(" id=\"%s\"", id);

    printf(">\n");
}

void html_endDiv() {
	printf("</div>\n");
}

int html_loadElement(const char *htmlFile) {
	FILE *fp = fopen(htmlFile, "r");
	if (fp == NULL)
		return -1;

	char c;

	while ((c = fgetc(fp)) != EOF) {
		printf("%c", c);
	}
	fclose(fp);
	return 0;
}

void html_print_footer(unsigned int next, unsigned int back, const char *fid, const char *content, const char *search) {
	html_startDiv(NULL, fid);
	printf("<center>\n");

	printf("<form method=\"get\">\n");
	printf("<input type=\"hidden\" name=\"page\" value=\"%d\">\n", back);
	printf("<input type=\"hidden\" name=\"content\" value=\"%s\">\n", content);
	if (search)
		printf("<input type=\"hidden\" name=\"search\" value=\"%s\">\n", search);
	printf("<button type=\"submit\">\n");
	printf("<h3>zur&uuml;ck</h3>\n");
	printf("</button>\n");
	printf("</form>\n");

	printf("<form method=\"get\">\n");
	printf("<input type=\"hidden\" name=\"page\" value=\"%d\">\n", next);
	printf("<input type=\"hidden\" name=\"content\" value=\"%s\">\n", content);
	if (search)
		printf("<input type=\"hidden\" name=\"search\" value=\"%s\">\n", search);
	printf("<button type=\"submit\">\n");
	printf("<h3>weiter</h3>\n");
	printf("</button>\n");
	printf("</form>\n");

	printf("</center>\n");
	html_endDiv();
}


void html_noResults() {
	printf("<center>Leider Keine Ergebnisse gefunden</center><br>\n");
}
